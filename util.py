import time
import socket
from telnetlib import Telnet


DEFAULT_HOST = '192.168.16.2'


def sensor_on(ip):
    sensor_off(ip)
    time.sleep(0.5)
    cmd = b'cd /opt\n ./sensor_server &\n'
    tn = Telnet(ip)
    tn.write(cmd)
    time.sleep(0.2)
    tn.read_very_eager()


def sensor_off(ip):
    with socket.socket() as sock:
        try:
            sock.connect((ip, 6172))
        except:
            pass
        else:
            sock.sendall(b'STOP\x00\x00\x00\x00')
    with Telnet(ip) as tn:
        kill = b'killall -9 sensor_server\n'
        tn.write(kill)
        tn.read_some()


def _tk_sensor_on():
    ip = host.get()
    sensor_on(ip)


def _tk_sensor_off():
    ip = host.get()
    sensor_off(ip)


if __name__ == '__main__':
    import tkinter
    root = tkinter.Tk()
    host = tkinter.StringVar(root)
    host.set(DEFAULT_HOST)
    tkinter.Button(root, text='Sensor On', command=_tk_sensor_on).pack()
    tkinter.Button(root, text='Sensor Off', command=_tk_sensor_off).pack()
    tkinter.Button(root, text='Exit', command=root.destroy).pack()
    root.mainloop()
