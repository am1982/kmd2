from itertools import product
from itertools import count
import asyncio
import argparse
import struct
import sys
import time
from datetime import datetime as dt
from threading import Thread
from typing import Union

import numpy as np
import ujson as json
import websockets
import zmq
import zmq.asyncio

import command
from util import sensor_on, sensor_off


class QueueManager:
    def __init__(self):
        self.queues = []

    def __iter__(self):
        for q in self.queues.copy():
            yield q

    def add(self) -> asyncio.Queue:
        q = asyncio.Queue()
        self.queues.append(q)
        return q

    def remove(self, q: asyncio.Queue) -> None:
        try:
            self.queues.remove(q)
        except ValueError:
            pass


class Frame:
    """
    Represents an incoming frame from the sensor.
    Iteration is used to unpack the chirps.
    """
    frame_counter = count(1)
    creation_time = None
    effective_spacing = 1e-5
    fake_azimuth = 0

    def __init__(
            self, topic, data: np.ndarray, receiver: int,
            resolution: float, fps: float, fft: bool,
            window_function: str, bandwidth: int,
            rx_gain: int, azimuth: Union[None, float]
            ):
        if isinstance(topic, bytes):
            self.topic = topic.decode()
        else:
            self.topic = topic
        self.data = data
        self.receiver = receiver
        self.resolution = resolution
        self.fps = fps
        self.sample_spacing = 1 / fps
        self.set_creation_time()
        self.every_nth_chirp = 256 or int(self.effective_spacing / self.sample_spacing / 256)
        self.fft = fft
        self.window_function = window_function
        self.bandwidth = bandwidth
        self.rx_gain = rx_gain
        self.azimuth = azimuth

    def __iter__(self):
        state = {
            'mode': 'fmcw',
            'window_function': self.window_function,
            'fft': self.fft,
            'bandwidth': self.bandwidth,
            'rx_gain': self.rx_gain,
            'receiver': self.receiver + 1,
            }
        frame = {
            'iData': [0.0], 'qData': [0.0],
            'resolution': self.resolution,
            'timestamp': 0.0,
            'iso_timestamp': '',
            'frame_id': next(self.frame_counter),
            'topic': self.topic,
            'compass': self.azimuth if self.azimuth is not None else self.fake_azimuth,
            'state': state
            }
        if self.topic == 'RADC':
            for chirp in self.data[self.receiver, ::self.every_nth_chirp]:
                if self.fft:
                    fft = np.fft.fft(chirp[..., 0] + 1j * chirp[..., 1])[1:128]
                    frame['iData'] = fft.real.tolist()
                    frame['qData'] = fft.imag.tolist()
                else:
                    frame['iData'] = chirp[..., 0].tolist()
                    frame['qData'] = chirp[..., 1].tolist()
                now = dt.utcnow()
                frame['timestamp'] = now.timestamp()
                frame['iso_timestamp'] = now.isoformat()
                self.__class__.fake_azimuth = (self.fake_azimuth + 1) % 360
                yield json.dumps(frame)
        elif self.topic == 'RMRD':
            frame['iData'] = self.data[:, 127].tolist()
            frame['qData'] = self.data[:, 127].tolist()
        now = dt.utcnow()
        frame['timestamp'] = now.timestamp()
        frame['iso_timestamp'] = now.isoformat()
        self.__class__.fake_azimuth = (self.fake_azimuth + 1) % 360
        yield json.dumps(frame)

    @classmethod
    def set_creation_time(cls):
        if cls.creation_time is None:
            cls.creation_time = time.time()
        else:
            now = time.time()
            delta_time = now - cls.creation_time
            cls.effective_spacing = delta_time / 256
            cls.creation_time = now


def delta_range(bandwidth: int) -> float:
    """
    bandwidth := frequency in MHz

    The calculation of range resolution
    depends on used samples and ignored samples.

    Current setting is:

    chirp_num_steps = 260
    chirp_num_steps_ignored = 4

    It returns the range resolution * 2
    """
    bandwidth *= 1e6  # MHz
    all_steps = 260
    ignored_steps = 4
    used_steps = all_steps - ignored_steps
    range_resolution = 150e6 * all_steps / (used_steps * bandwidth)
    # user interface divides the range resolution by 2
    return range_resolution * 4


def handle_data(topic: bytes, data: bytes) -> np.ndarray:
    if topic == b'RADC':
        return np.frombuffer(data, dtype=np.uint16).reshape((3, 256, 256, 2))
    elif topic == b'RMRD':
        return np.frombuffer(data, dtype=np.uint32).reshape((256, 256))


async def sensor_init(writer: asyncio.StreamWriter, bandwidth: int, rx_gain: int):
    writer.write(command.init())
    await writer.drain()
    for disable_topic in ('DONE', 'RMRD', 'PDAT', 'TDAT', 'RPRM', 'PPRM', 'GBYE'):
        writer.write(command.dsf0(disable_topic))
    writer.write(command.rsbw(bandwidth))
    writer.write(command.rsrg(rx_gain))
    writer.write(command.dsf1('RADC'))
    await writer.drain()


async def command_executor(cmd: str, writer: asyncio.StreamWriter):
    """
    This functions supports only the commands:
    RSBW to set the the bandwidth (100-10000 MHz)
    RSRD to set the RX Gain (0 - 40 dB)
    """
    if cmd.strip().upper() == 'STOP':
        writer.write(command.stop())
        await writer.drain()
        sys.exit(0)
    try:
        cmd, value = cmd.strip().upper().split()
        value = int(value)
    except ValueError:
        print(
            'The supplied command has the wrong format.',
            'RSBW <0 - 1000>',
            'RSRG <0 - 40>', sep='\n'
        )
        return
    try:
        if cmd == 'RSBW':
            cmd_data = command.rsbw(value)
        elif cmd == 'RSRG':
            cmd_data = command.rsrg(value)
        else:
            return
        writer.write(cmd_data)
        await writer.drain()
    except Exception as e:
        print(e, file=sys.stderr)
    return cmd, value


async def sensor_handler(
        ip: str, port: int, receiver: int, bandwidth: int, rx_gain: int,
        zmq_sock: zmq.asyncio.Socket, zmq_topic: bytes,
        command_queue: asyncio.Queue, fps: float, fft: bool,
        window_function: str, zmq_compass,
        ):
    st_psize = struct.Struct('<I')
    resolution = delta_range(bandwidth)
    reader, writer = await asyncio.open_connection(ip, port, limit=2 * 1024 ** 2)
    await sensor_init(writer, bandwidth, rx_gain)
    azimuth = None
    while True:
        # receiving commands via stdin
        if not command_queue.empty():
            # we don't want to block here
            cmd = command_queue.get_nowait()
            action = await command_executor(cmd, writer)
            if action:
                name, value = action
                if name == 'RSBW':
                    if 100 <= value <= 1000:
                        bandwidth = value
                        resolution = delta_range(value)
                if name == 'RSRG':
                    if 0 <= value <= 40:
                        rx_gain = value
            command_queue.task_done()
        # receiving data from sensor
        # get the topic as first, which is always 4 bytes long
        topic = await reader.readexactly(4)
        # followed always by a uint32 which specifies the size
        # of the payload
        (psize,) = st_psize.unpack(await reader.readexactly(4))
        # receive exactly the size, which is specified by psize
        data = await reader.readexactly(psize)
        # binary data is now complete
        topics = [b'RADC', b'RMRD']
        if data and any(topic == t for t in topics):
            data = handle_data(topic, data)
            # receiving compass data via zmq noblocking
            if zmq_compass:
                # receive until the queue is empty, which will rise an Exception
                while True:
                    try:
                        _, azimuth = await zmq_compass.recv_multipart(flags=zmq.NOBLOCK)
                    except:
                        break
            frame = Frame(
                topic, data,
                receiver=receiver, resolution=resolution,
                fps=fps, fft=fft, window_function=window_function,
                bandwidth=bandwidth, rx_gain=rx_gain, azimuth=azimuth,
                )
            iterator = product(frame, queues)
            for single_frame, q in iterator:
                await q.put(single_frame)
            if zmq_sock:
                for single_frame in frame:
                    await zmq_sock.send_multipart([zmq_topic, single_frame.encode()])


async def ws_handler(websocket, path):
    if path != '/data':
        await websocket.send({'error': f'path {path} is not allowed'})
        return
    q = queues.add()
    while True:
        frame = await q.get()
        try:
            await websocket.send(frame)
        except:
            queues.remove(q)


def command_reader(queue: asyncio.Queue):
    while True:
        command = sys.stdin.readline().strip()
        queue.put_nowait(command)


def main(*,
         sensor_ip: str, sensor_port: int,
         ws_ip: str, ws_port: int,
         receiver: int, bandwidth: int,
         rx_gain: int,
         zmq_sock: zmq.asyncio.Socket,
         zmq_topic: bytes,
         cmd_queue: asyncio.Queue,
         fps: float, fft: bool, window_function: str,
         zmq_compass,
         ):
    sensor_on(sensor_ip)
    loop = asyncio.get_event_loop()
    start_ws_server = websockets.serve(ws_handler, ws_ip, ws_port, ping_interval=None, ping_timeout=None)
    start_sensor = sensor_handler(
        sensor_ip, sensor_port, receiver,
        bandwidth, rx_gain, zmq_sock, zmq_topic,
        cmd_queue, fps, fft, window_function, zmq_compass)
    asyncio.ensure_future(start_ws_server)
    asyncio.ensure_future(start_sensor)
    Thread(target=command_reader, args=[cmd_queue], daemon=True).start()
    loop.run_forever()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Websocket Server for K-MD2')
    parser.add_argument('--sensor-ip', required=False, default='192.168.16.2', help='IP of K-MD2 sensor')
    parser.add_argument('--sensor-port', required=False, default=6172, type=int, help='Port of K-MD2 sensor')
    parser.add_argument('--fft', required=False, default=False, action='store_true', help='Apply fft')
    parser.add_argument(
        '--window_function', required=False,
        default='hanning',
        choices=['hanning', 'hamming', 'blackman'],
        help='Applied window function before the fft is applied.'
        )
    parser.add_argument('--ws-ip', required=False, default='0.0.0.0', help='IP of websocket server')
    parser.add_argument('--ws-port', required=False, default=8888, type=int, help='Port of websocket server')
    parser.add_argument('--rsbw', required=False, default=750, type=int, help='Set the bandwitdh 100MHz - 1000MHz')
    parser.add_argument('--rsrg', required=False, default=10, type=int, help='Set the RX Gain in dB 0-40')
    parser.add_argument('--receiver', required=False, default=1, type=int, help='Select Receiver 1, 2 or 3')
    parser.add_argument(
        '--zmq-type', required=False, default='server',
        choices=('server', 'client'),
        help='Choose if zmq should run a server or client'
        )
    parser.add_argument(
        '--zmq-address', required=False, default=None,
        help='If address for zmq is supplied, then zmq is publishing on this address'
        )
    parser.add_argument(
        '--zmq-topic', required=False, default=b'KMD2', type=bytes,
        help='Topic which is used for the zmq publisher')
    parser.add_argument(
        '--zmq-compass', required=False, default='tcp://127.0.0.1:5050',
        help='ZMQ compass subscriber address',
)
    parser.add_argument('--fps', required=False, type=float, default=30, help='Limit sending of sensor by <fps>')
    args = parser.parse_args()
    cmd_queue = asyncio.Queue()
    queues = QueueManager()
    # create a zmq context if the server should send
    # or receive data via zmq
    if args.zmq_address or args.zmq_compass:
        ctx = zmq.asyncio.Context.instance()
    # zmq for frame publishing
    if args.zmq_address:
        sock = ctx.socket(zmq.PUB)
        if args.zmq_type == 'server':
            sock.bind(args.zmq_address)
        elif args.zmq_type == 'client':
            sock.connect(args.zmq_address)
    else:
        sock = None
    # zmq to receive compass data (subscriber)
    if args.zmq_compass:
        zmq_compass = ctx.socket(zmq.SUB)
        zmq_compass.connect(args.zmq_compass)
        zmq_compass.subscribe(b'CMPS')
    else:
        zmq_compass = None
    main(
        sensor_ip=args.sensor_ip, sensor_port=args.sensor_port,
        ws_ip=args.ws_ip, ws_port=args.ws_port,
        bandwidth=args.rsbw, rx_gain=args.rsrg,
        receiver=args.receiver - 1,
        zmq_sock=sock, zmq_topic=args.zmq_topic,
        cmd_queue=cmd_queue, fps=args.fps, fft=args.fft,
        window_function=args.window_function,
        zmq_compass=zmq_compass,
    )
