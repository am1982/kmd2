import struct
import numpy as np


class BaseParser:

    def __init__(self):
        selector = {}
        for name in dir(self):
            obj = getattr(self, name)
            if name.startswith('parse_') and callable(obj):
                topic = name.split('_')[1].upper()
                selector[topic] = obj
        self.selector = selector

    def parse(self, topic, data):
        if isinstance(topic, bytes):
            topic = topic.decode()
        method = self.selector.get(topic)
        if method is not None:
            return method(data)
        else:
            return []


class Parser(BaseParser):
    st_radc = struct.Struct('<393216H')
    st_rmrd = struct.Struct('<262144I')
    st_pdat = struct.Struct('<2H2h2H')
    st_tdat = struct.Struct('<2i9f')
    st_rprm = struct.Struct('<6H')
    st_pprm = struct.Struct('<2I2Hf11Hh4H2f')

    def parse_radc(self, data):
        data = self.st_radc.unpack(data)
        return np.array(data, dtype=np.uint16).reshape((3, 256, 256, 2))

    def parse_rmrd(self, data):
        data = self.st_rmrd.unpack(data)
        return np.array(data, dtype=np.uint32).reshape((256, 256))

    def parse_pdat(self, data):
        size = len(data) // 12
        result = []
        for i in range(size):
            result.append(
                self.st_pdat.unpack(
                    data[12 * i: 12 * i + 12]
                ))
        return result

    def parse_tdat(self, data):
        size = len(data) // 44
        result = []
        for i in range(size):
            result.append(
                self.st_tdat.unpack(
                    data[44 * i: 44 * i + 44]
                ))
        return result

    def parse_rprm(self, data):
        return self.st_rprm.unpack(data)

    def parse_pprm(self, data):
        return self.st_pprm.unpack(data)


parser = Parser()
parse = parser.parse
