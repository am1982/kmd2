import struct


ERROR = '{} must be in range of {}-{}'


class CommandError(Exception):
    pass


def in_range(name, value, min_val, max_val):
    if not min_val <= value <= max_val:
        raise CommandError(ERROR.format(name, min_val, max_val))


def init():
    """
    Run the initialization sequence and load the configuration files
    """
    return bytearray(struct.pack('<4sI', b'INIT', 0))


def rsid(clk):
    """
    Radar initial delay [clk]
    """
    in_range('clk', clk, 0, 32768)
    return struct.pack('<4s2I', b'RSID', 4, clk)


def rssf(freq):
    """
    Radar start frequency [MHz] 23800 – 24800
    """
    in_range('freq', freq, 23800, 24800)
    return struct.pack('<4s2I', b'RSSF', 4, freq)


def rsbw(freq):
    """
    Radar bandwidth [MHz] 100 – 1000
    """
    in_range('freq', freq, 100, 1000)
    return struct.pack('<4s2I', b'RSBW', 4, freq)


def rsrg(dB):
    """
    Radar receiver gain [dB] 0 – 80
    """
    in_range('dB', dB, 0, 80)
    return struct.pack('<4s2I', b'RSRG', 4, dB)


def psmc(rx1: bool, rx2: bool, rx3: bool) -> bytes:
    """
    Receivers to include in mean range-doppler map 0x01 = RX1 0x02 = RX2 0x04 = RX3
    """
    receivers = rx1 | rx2 << 1 | rx3 << 2
    return struct.pack('<4s2I', b'PSMC', 4, receivers)


def pspt(detection_threshold: int) -> bytes:
    """
    Processor peak detection threshold 0 – 32768
    """
    in_range('detection_threshold', detection_threshold, 0, 32768)
    return struct.pack('<4s2I', detection_threshold)


def psrc(range_threshold: float) -> bytes:
    """
    Processor range compensation for threshold 0.0 – 5.0
    """
    in_range('range_threshold', range_threshold, 0.0, 5.0)
    return struct.pack('<4sIf', b'PSRC', 4, range_threshold)


def psnp(max_peaks: int) -> bytes:
    """
    Processor maximum number of peaks 0-200
    """
    in_range('max_peaks', max_peaks, 0, 200)
    return struct.pack('<4sII', b'PSNP', 4, max_peaks)


def psbu(update_rate: int) -> bytes:
    if update_rate not in (0, 2, 4, 8, 16, 32, 64, 128, 256):
        raise CommandError('update_rate is not valid')
    return struct.pack('<4s2I', b'PSBU', 4, update_rate)


def psbr(peak_det_min: int) -> bytearray:
    """
    Processor peak detection minimum range [bin]
    """
    in_range('peak_det_min', peak_det_min, 2, 254)
    buffer = bytearray(b'PSBR')
    buffer.extend(struct.pack('<B', peak_det_min))
    return buffer


def pstr(peak_det_max: int) -> bytearray:
    """
    Processor peak detection maximum range [bin]
    """
    in_range('peak_det_max', peak_det_max, 2, 254)
    buffer = bytearray(b'PSTR')
    buffer.extend(struct.pack('<B', peak_det_max))
    return buffer


def psbs(speed_det_min: int) -> bytearray:
    """
    Processor peak detection minimum speed [bin]
    """
    in_range('speed_det_min', speed_det_min, 0, 126)
    buffer = bytearray(b'PSBS')
    buffer.extend(struct.pack('<B', speed_det_min))
    return buffer


def psts(speed_det_max: int) -> bytearray:
    """
    Processor peak detection maximum speed [bin]
    """
    in_range('speed_det_max', speed_det_max, 0, 126)
    buffer = bytearray(b'PSTS')
    buffer.extend(struct.pack('<B', speed_det_max))
    return buffer


def pssm(active: bool) -> bytearray:
    """
    Processor smooth mean range-doppler map
    """
    buffer = bytearray(b'PSSM')
    buffer.extend(struct.pack('<B', active))
    return buffer


def psnt(max_tracks: int) -> bytearray:
    """
    Processor maximum number of tracks to report
    """
    in_range('max_tracks', max_tracks, 0, 200)
    buffer = bytearray(b'PSNT')
    buffer.extend(struct.pack('<B', max_tracks))
    return buffer


def psrj(max_range_jitter: int) -> bytearray:
    """
    Processor maximum range jitter [bin]
    """
    in_range('max_range_jitter', max_range_jitter, 0, 10)
    buffer = bytearray(b'PSRJ')
    buffer.extend(struct.pack('<B', max_range_jitter))
    return buffer


def pssj(max_speed_jitter: int) -> bytearray:
    """
    Processor maximum speed jitter [bin]
    """
    in_range('max_speed_jitter', max_speed_jitter, 0, 10)
    buffer = bytearray(b'PSSJ')
    buffer.extend(struct.pack('<B', max_speed_jitter))
    return buffer


def psbl(min_track_life: int) -> bytearray:
    """
    Processor minimum track life [frame]
    """
    in_range('min_track_life', min_track_life, 0, 255)
    buffer = bytearray(b'PSBL')
    buffer.extend(struct.pack('<B', min_track_life))
    return buffer


def pstl(max_track_life: int) -> bytearray:
    """
    Processor maximum track life [frame]
    """
    in_range('max_track_life', max_track_life, 0, 255)
    buffer = bytearray(b'PSBL')
    buffer.extend(struct.pack('<B', max_track_life))
    return buffer


def psth(hist_length: int) -> bytearray:
    """
    Processor length of history for tracking [frame]
    """
    in_range('hist_length', hist_length, 0, 255)
    buffer = bytearray(b'PSTH')
    buffer.extend(struct.pack('<B', hist_length))
    return buffer


def psso(report_stationary: bool) -> bytearray:
    """
    Processor report stationary objects
    """
    buffer = bytearray(b'PSSO')
    buffer.extend(struct.pack('<B', report_stationary))
    return buffer


def pscs(const_speed: bool) -> bytearray:
    """
    Processor assume constant speed for tracking
    """
    buffer = bytearray(b'PSCS')
    buffer.extend(struct.pack('<B', const_speed))
    return buffer


def dsf0(topic: str) -> bytes:
    """
    Disable topic
    """
    if isinstance(topic, str):
        topic = topic.encode()
    return struct.pack('<4sI4s', b'DSF0', 4, topic)


def dsf1(topic: str) -> bytes:
    """
    Enable topic
    """
    if isinstance(topic, str):
        topic = topic.encode()
    return struct.pack('<4sI4s', b'DSF1', 4, topic)


def gbye() -> bytes:
    """
    Disconnect but leave the sensor server running
    """
    return struct.pack('<4sI', b'GBYE', 0)


def stop() -> bytes:
    """
    Stop the sensor server
    """
    return struct.pack('<4sI', b'STOP', 0)
