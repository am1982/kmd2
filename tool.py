import time
import socket
from telnetlib import Telnet


HOST = '192.168.0.201'


def on():
    off()
    time.sleep(0.5)
    print('Switching sensor On')
    cmd = b'cd /opt\n ./sensor_server &\n'
    tn = Telnet(HOST)
    tn.write(cmd)
    time.sleep(0.2)
    print(tn.read_very_eager().decode())


def off():
    print('Switching sensor off')
    sock = socket.socket()
    try:
        sock.connect((HOST, 6172))
    except:
        print('Could not connect to sensor, trying telnet')
    else:
        sock.sendall(b'STOP\x00\x00\x00\x00')
    finally:
        sock.close()
        return
    tn = Telnet(HOST)
    kill = b'killall -9 sensor_server\n'
    tn.write(kill)
    tn.read_some()
    tn.close()

